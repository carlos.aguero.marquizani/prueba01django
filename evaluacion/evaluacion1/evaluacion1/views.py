from django.http import HttpResponse

#vista1
def bienvenida(request):
    return HttpResponse("¡Esta es la primera vista para la evaluación!")

#vista2
def categoriaEdad(request, edad):
    if edad >=18:
        categoria = "mayor de 18 años, puede sacar licencia."
    else:
        categoria = "menor de 17 años, no puede sacar licencia."
    resultado = "Ud. es %s" %categoria
    return HttpResponse(resultado)

#vista3
def vistaJubilacion(request, sexo, edad):
    if sexo == "hombre" and edad >= 65:
        return HttpResponse("Puede jubilarse")
    elif sexo == "mujer" and edad >= 60:
        return HttpResponse("Puede jubilarse")
    else:
        return HttpResponse("No puede jubilarse")